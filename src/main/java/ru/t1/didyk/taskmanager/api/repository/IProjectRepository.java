package ru.t1.didyk.taskmanager.api.repository;

import ru.t1.didyk.taskmanager.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

}
