package ru.t1.didyk.taskmanager.repository;

import ru.t1.didyk.taskmanager.api.repository.ITaskRepository;
import ru.t1.didyk.taskmanager.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return models
                .stream()
                .filter(task -> task.getProjectId() != null)
                .filter(task -> task.getProjectId().equals(projectId))
                .filter(task -> task.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

}
