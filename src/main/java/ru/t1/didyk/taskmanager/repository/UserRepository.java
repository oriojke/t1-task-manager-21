package ru.t1.didyk.taskmanager.repository;

import ru.t1.didyk.taskmanager.api.repository.IUserRepository;
import ru.t1.didyk.taskmanager.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findById(final String id) {
        return models
                .stream()
                .filter(user -> id.equals(user.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public User findByLogin(final String login) {
        return models
                .stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return models
                .stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public Boolean isLoginExists(final String login) {
        return models
                .stream()
                .anyMatch(user -> login.equals(user.getLogin()));
    }

    @Override
    public Boolean isEmailExists(final String email) {
        return models
                .stream()
                .anyMatch(user -> email.equals(user.getEmail()));
    }

}