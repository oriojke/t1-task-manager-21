package ru.t1.didyk.taskmanager.command.task;

import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {
    @Override
    public String getDescription() {
        return "Change task status by index.";
    }

    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusByIndex(userId, index, status);
    }

}
