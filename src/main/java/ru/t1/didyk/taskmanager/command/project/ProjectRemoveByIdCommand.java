package ru.t1.didyk.taskmanager.command.project;

import ru.t1.didyk.taskmanager.model.Project;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(userId, id);
        getProjectTaskService().removeProjectById(userId, id);
    }

}
