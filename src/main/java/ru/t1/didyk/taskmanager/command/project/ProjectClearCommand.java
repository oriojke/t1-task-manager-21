package ru.t1.didyk.taskmanager.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear(userId);
    }

}
