package ru.t1.didyk.taskmanager.command.project;

import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
    }

}
