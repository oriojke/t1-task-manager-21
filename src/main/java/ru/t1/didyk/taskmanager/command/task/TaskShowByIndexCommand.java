package ru.t1.didyk.taskmanager.command.task;

import ru.t1.didyk.taskmanager.model.Task;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {
    @Override
    public String getDescription() {
        return "Show task by index.";
    }

    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = getTaskService().findOneByIndex(userId, index);
        showTask(task);
    }

}
