package ru.t1.didyk.taskmanager.command.user;

import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "View profile of current user.";
    }

    @Override
    public String getName() {
        return "view-user-profile";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW USER PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
