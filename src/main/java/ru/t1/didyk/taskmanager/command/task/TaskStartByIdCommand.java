package ru.t1.didyk.taskmanager.command.task;

import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {
    @Override
    public String getDescription() {
        return "Start task by id.";
    }

    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

}
