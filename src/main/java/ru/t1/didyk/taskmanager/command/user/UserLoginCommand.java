package ru.t1.didyk.taskmanager.command.user;

import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "User login.";
    }

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
