package ru.t1.didyk.taskmanager.command.user;

import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "Change password of current user.";
    }

    @Override
    public String getName() {
        return "change-user-password";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserID();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
