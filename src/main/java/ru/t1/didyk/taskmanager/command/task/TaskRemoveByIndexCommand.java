package ru.t1.didyk.taskmanager.command.task;

import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {
    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().removeByIndex(userId, index);
    }

}
