package ru.t1.didyk.taskmanager.command.task;

import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {
    @Override
    public String getDescription() {
        return "Update task by index.";
    }

    @Override
    public String getName() {
        return "task-update-by-index";
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[UPDATE TASK BY INDEX");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getTaskService().updateByIndex(userId, index, name, description);
    }

}
