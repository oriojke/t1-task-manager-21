package ru.t1.didyk.taskmanager.command.user;

import ru.t1.didyk.taskmanager.api.service.IAuthService;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.model.User;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "Register user.";
    }

    @Override
    public String getName() {
        return "user-registry";
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = serviceLocator.getAuthService();
        User user = authService.registry(login, password, email);
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
